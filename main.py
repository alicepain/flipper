import pygame
import pymunk
import sys
from random import randint

def createBall(space, pos, rad, col):
    body = pymunk.Body(1,100,body_type = pymunk.Body.DYNAMIC) #Body(mass, inertia, body_type)
    body.position = pos
    body.radius = rad
    body.color = col
    shape = pymunk.Circle(body,body.radius)
    space.add(body,shape)
    return shape

def createObstacle(space,pos,rad):
    body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body.position = pos
    body.radius = rad
    shape = pymunk.Circle(body,rad)
    space.add(body,shape)
    return shape

def createPlatform(space, start_pos, end_pos, width):
    body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body.start_pos = start_pos
    body.end_pos = end_pos
    body.width = width
    shape = pymunk.Segment(body, start_pos,end_pos,width)
    space.add(body,shape)
    return shape

def drawBalls(balls):
    for ball in balls:
        pos_x = int(ball.body.position.x)
        pos_y = int(ball.body.position.y)
        rad = ball.body.radius
        col = ball.body.color
        pygame.draw.circle(game.screen,col,(pos_x,pos_y),rad)

def drawObstacles(obstacles):
    for obstacle in obstacles:
        pos_x = int(obstacle.body.position.x)
        pos_y = int(obstacle.body.position.y)
        rad = obstacle.body.radius
        pygame.draw.circle(game.screen,(200,200,200),(pos_x,pos_y),rad)

def drawPlatforms(platforms):
    for platform in platforms:
        s_pos_x = int(platform.body.start_pos[0])
        s_pos_y = int(platform.body.start_pos[1])
        e_pos_x = int(platform.body.end_pos[0])
        e_pos_y = int(platform.body.end_pos[1])
        width = platform.body.width
        pygame.draw.line(game.screen, (0,0,0),(s_pos_x,s_pos_y), (e_pos_x,e_pos_y), width)

class Game():

    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((800,800))
        self.clock = pygame.time.Clock()
        self.space = pymunk.Space()
        self.space.gravity = (0,300)
        self.balls = []
        self.obstacles = [createObstacle(self.space,(i,140),15) for i in range(0,900,100)] + [createObstacle(self.space,(i,70),15) for i in range(50,900,100)] + [createObstacle(self.space,(i,210),15) for i in range(50,900,100)]
        self.platforms = [createPlatform(self.space, (0,300),(300,450),5), createPlatform(self.space, (600,500),(800,400),5), createPlatform(self.space, (400,700),(650,700),5)] 

    def draw(self):
        self.screen.fill((255,255,255))
        drawPlatforms(self.platforms)
        drawBalls(self.balls)
        drawObstacles(self.obstacles)
        pygame.display.update()

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    self.balls.append(createBall(self.space,event.pos,randint(5,30),(randint(0,255),randint(0,255),randint(0,255))))
            self.space.step(1/50)
            self.draw()
            self.clock.tick(120)

if __name__ == "__main__":
    game = Game()
    game.run()
       
